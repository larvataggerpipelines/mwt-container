CMD:=podman
VERSION:=0.2.1
TAG:=quay.io/flaur/mwt-container

.PHONY: apptainer all build tag push login

apptainer:
	apptainer build mwt-container-$(VERSION).sif docker://$(TAG):$(VERSION)

all: build tag login push

build:
	$(CMD) build --no-cache --pull -t mwt-container $(CURDIR)

tag:
	$(CMD) tag mwt-container $(TAG):$(VERSION)
	$(CMD) tag mwt-container $(TAG):latest

push:
	$(CMD) push $(TAG):$(VERSION)
	$(CMD) push $(TAG):latest

login:
	$(CMD) login quay.io
