FROM registry.opensuse.org/opensuse/tumbleweed:latest
# rolling release; remember to pull before

ENV JULIA_DEPOT_PATH=/usr/local/share/julia

RUN mkdir -p /app/bin \
 && cd /app \
 && zypper refresh \
 && zypper dup -y \
 && zypper install -y git make gcc-c++ opencv-devel java-21-openjdk-headless python310 gstreamer-1.20-plugin-openh264 ffmpeg-6 \
 && python3.10 -m venv jill \
 && cd jill \
 && . bin/activate \
 && pip install jill \
 && jill install 1.10.8 --confirm \
 && deactivate \
 && mkdir -p "$JULIA_DEPOT_PATH/logs" \
 && rm -f "$JULIA_DEPOT_PATH/logs/manifest_usage.toml" \
 && cd /app \
 && git clone --depth 1 https://gitlab.com/larvataggerpipelines/mwt-cli \
 && cd mwt-cli \
 && make \
 && julia --project=. -e 'using LazyArtifacts; artifact"Chore/Chore.jar"' \
 && cd /app \
 && git clone --depth 1 --branch dev https://github.com/francoislaurent/larva-tagger-tune lilly-tune \
 && python3.10 -m venv . \
 && . bin/activate \
 && pip install -r lilly-tune/requirements.txt

ENV JULIA_DEPOT_PATH="/tmp:$JULIA_DEPOT_PATH"

COPY bin/rerun_best_trial.py /app/bin/
COPY bin/mwt_optimizer.sh /app/bin/
COPY bin/ffv1_encode.sh /app/bin/

ENTRYPOINT ["/app/bin/mwt_optimizer.sh"]
