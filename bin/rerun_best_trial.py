import joblib
import subprocess
import os
import sys
import glob

def main():
    data = sys.argv[1]
    assay = sys.argv[2]
    mwt = sys.argv[3]

    command = ['julia', f'--project={mwt}', os.path.join(mwt, 'src/mwt-cli.jl'),
            f"{data}/raw/{assay}.avi", f"{data}/interim/{assay}"]

    date_time = None
    frame_rate = '30'
    pixel_size = '0.073'
    for arg in sys.argv[4:]:
        if arg.startswith('--static-args='):
            arg = arg.split('=', 1)[1].strip('\' "').split()
            command.extend(arg)
        elif arg.startswith('--date-time='):
            date_time = arg.split('=', 1)[1]
        elif arg.startswith('--fps='):
            frame_rate = arg.split('=', 1)[1]
        elif arg.startswith('--pixel_size='):
            pixel_size = arg.split('=', 1)[1]

    if date_time is None:
        # for backward compatibility
        y, m, d, hms, _ = os.path.basename(assay).split('_')[-1].split('-')
        date_time = f"{y}{m}{d}_{hms}"

    study_file = glob.glob(f"{data}/interim/{assay}/Parameter_Optimization_MWT_*.pkl")
    assert len(study_file) == 1
    study_file = study_file[0]

    study = joblib.load(study_file)
    params = study.best_trial.params
    prm = lambda p: str(params[p])

    hyperparams = ['--frame-rate', frame_rate,
                '--pixel-thresholds', prm('pixel-thr1'), prm('pixel-thr2'),
                '--size-thresholds', prm('size-thr1'), prm('size-thr2'),
                '--pixel-size', pixel_size, '--date-time', date_time]
    command.extend(hyperparams)

    subprocess.run(command)

if __name__ == '__main__':
    main()
