#!/bin/sh

if [ -z $1 ]; then
echo "$0 <avifile>"
else

pushd `dirname "$1"`

# file name or path without extension
movie=`basename "$1" .avi`

suffix=_ffv1
args="-vcodec ffv1 -level 3 -context 1 -threads 16"

eval "ffmpeg -i ${movie}.avi ${args} -pass 1 -passlogfile ${movie}${suffix} -f nut -y /dev/null"
eval "ffmpeg -i ${movie}.avi ${args} -pass 2 -passlogfile ${movie}${suffix} ${movie}${suffix}.avi"

# optional
rm ${movie}${suffix}*.log

popd

# print the name of the generated file as the last line of output,
# so that it can be extracted as return value
echo "${movie}${suffix}.avi"

fi
