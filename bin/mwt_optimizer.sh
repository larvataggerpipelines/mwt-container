#!/usr/bin/env bash

# Usage: $0 path/to/movie
#        $0 path/to/movie [ntrials] [--date-time DATE_TIME] [--target-larvae-nr FILENAME]
#                         [--fps FPS] [--pixel_size SIZE] [--nr_processes N]
#                         [--compress] [--debug] [...]
#
# Path `path/to/movie` must be relative to `/data/raw` in the container.
# Optional argument `ntrials` corresponds to larva-tagger-tune's argument `--nr_trials`.
# Options `--fps`, `--pixel_size` and `--nr_processes` are passed to larva-tagger-tune as is.
# The options that take values, such as `--date-time`, expect a space (NOT an equal sign) between the option and its value.
# [...] denotes optional arguments to mwt-cli.

# Assumptions:
# * the raw data repository is mounted as `/data/raw`;
# * the directory containing the `target_larvae_nr` csv file is mounted as `/data/external`;
# * an interim data (or temporary) directory is mounted as `/data/interim`;
# * the output repository is mounted as `/data/processed`;
# * the video files can be found in some directory below `/data/raw`, not as direct children of `/data/raw`.
# The file/directory structure below `/data/processed` is similar to that of `/data/raw`,
# with the video files replaced by directories with the same name, without the file extension.

parent_dir=$(dirname $1)
# remove file extension if any
video_name=$(basename $1 .avi)
shift
assay=$parent_dir/$video_name
working_dir=/app
mwt_dir=$working_dir/mwt-cli
data_dir=/data
video_dir=$data_dir/raw/$parent_dir
target_larvae_nr=$(ls $data_dir/external/*.csv | head -n1)
output_dir=$data_dir/interim/$assay
processed_dir=$data_dir/processed/$assay

if [ -z $1 ] || [ "${1:0:1}" = "-" ]; then
  trials=100
else
  trials=$1
  shift
fi

common_args=
mwt_args=
optim_args=
rerun_args=
while [ -n "$1" ]; do
  if [ "$1" = "--debug" ]; then
    optim_args="$optim_args $1"
  elif [ "$1" = "--compress" ]; then
    optim_args="$optim_args $2"
  elif [ "$1" = "--datetime" -o "$1" = "--date-time" -o "$1" = "--date_time" ]; then
    rerun_args="$rerun_args --date-time=$2"
    shift
  elif [ "$1" = "--fps" -o "$1" = "--pixel_size" -o "$1" = "--nr_processes" ]; then
    common_args="$common_args $1=$2"
    shift
  elif [ "$1" = "--target-larvae-nr" ]; then
    target_larvae_nr=/data/external/$2
    shift
  elif [ -n "$mwt_args" ]; then
    mwt_args="$mwt_args $1"
  else
    mwt_args=$1
  fi
  shift
done

if [ -n "$mwt_args" ]; then
  mwt_args=" --static-args='$mwt_args'"
fi

if ! [ -n "$rerun_args" ]; then
  echo "WARNING: missing date and time of recording; consider passing option --date-time <yyyymmdd_HHMMSS>"
fi

cd $working_dir
source bin/activate

if [ -f $output_dir/*.pkl ]; then
  echo "INFO: .pkl file found; consider deleting the interim directory"
else

echo """\
python lilly-tune/ParameterOptimization.py MWT $working_dir $video_dir $target_larvae_nr \\
	--video_names=${video_name}.avi --output_dir=$output_dir --nr_trials=$trials$common_args$optim_args$mwt_args 2>&1\
"""
python lilly-tune/ParameterOptimization.py MWT $working_dir $video_dir $target_larvae_nr \
	--video_names=${video_name}.avi --output_dir=$output_dir --nr_trials=$trials$common_args$optim_args$mwt_args 2>&1

#find $output_dir -type d | tail -n+2 | xargs rm -rf
rm -rf $output_dir/$video_name
fi
ls $output_dir

echo """\
python bin/rerun_best_trial.py $data_dir $assay $mwt_dir $common_args$rerun_args$mwt_args \
"""
python bin/rerun_best_trial.py $data_dir $assay $mwt_dir $common_args$rerun_args$mwt_args

mkdir -p $processed_dir
new_output_dir=$(find $output_dir -type d | tail -n1)
cp $new_output_dir/20* $processed_dir/

