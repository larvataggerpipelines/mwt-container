# `mwt-container`

This project builds Docker/Podman/Singularity/Apptainer images that ship with
[`mwt-cli`](https://gitlab.com/larvataggerpipelines/mwt-cli) and
[`larva-tagger-tune`](https://github.com/Lilly-May/larva-tagger-tune)
for headless tracking of *Drosophila* larvae in avi files.

MWT tracking parameters are fitted based on the expected number of moving larvae, provided in a csv file.
This strategy of approaching a predefined number of moving larvae may perform well only if all the larvae are visible during the entire recording.

## Availability

The generated Docker/Podman images are made available on quay.io, and can be used
simply pulling from [`quay.io/flaur/mwt-container`](https://quay.io/repository/flaur/mwt-container?tab=info).

## Passing data to/fro the container

Several data directories must be bound to specific mount points in the container.

The raw data repository with video files must be bound to `/data/raw`. Video files can be found in subdirectories.

The output data repository where spine/outline files are dumped must be bound to `/data/processed`.
For an input `dir/assay.avi` file, a corresponding `dir/assay` directory will be created in that output location.

An additional directory is expected, for intermediate files to be written. This directory must be bound to `/data/interim`.
Similarly to `/data/processed`, a directory corresponding to the input avi file will be created. This directory will contain a pkl file with information related to parameter optimization.

Last but not least, the parent directory of the csv file that specifies the target numbers of larvae must be bound to `/data/external`.
The csv file should preferably be the only csv file in that directory.
Otherwise, its name (or path relative to the `/data/external` directory) can be made explicit with argument `--target-larvae-nr`.

> `--target-larvae-nr` was added in version 0.2.

## Example usage

Example usage with the docker command, assuming the data are arranged as described above in a local *data* directory:
```
docker run --rm --mount "type=bind,src=`realpath data`,target=/data" quay.io/flaur/mwt-container <movie-file-name> --date-time <time-of-recording>
```
Note the data can be arranged otherwise and the required subdirectories mounted individually with multiple `--mount` arguments.

> `realpath` may not be available per default on macOS. It is provided by the *coreutils* package.
> It is not required, though. Specify a canonicalized absolute path after `src=`.

`--date-time` should be in the `yyyymmdd_HHMMSS` format. As hinted, it is supposed to give the date and time of the recording. To set `--date-time` to the current time instead, pass `` `date +%Y%m%d_%H%M%S` ``

> `--date-time` was added in version 0.2.

Additional options are:
* `--debug`  (since 0.2)
* `--background=dark`  (since 0.2)
* `--compress`  (since 0.2)
* `--target-larvae-nr` (since 0.2)

## Additional tools

`mwt-container` can also compress videos with the [FFV1 lossless compression algorithm](https://en.wikipedia.org/wiki/FFV1):
```
docker run --rm --mount "type=bind,src=`pwd`,target=/data" --entrypoint /app/bin/ffv1_encode.sh quay.io/flaur/mwt-container /data/<movie-file-name>
```
In the above example, a `<movie-file-name>` file in the current directory is processed and a new movie file is created, with an extra `_ffv1` suffix right before the `.avi` extension.
